$(document).ready(function() {

	//Select first tab when it starts
	$('#tabs').tabs();


	//Set animation speed for the notification
	$.fx.speeds._default = 1000;

	//Set the notification hiden with some effects
	$('#time-up').dialog({
		autoOpen: false,
		show: 'drop',
		hide: 'clip'
	});

	//Function to display a notification
	function noticeTimeUp() {
		$('#time-up').dialog('open');
		return false;
	}

	//Update slider handel value
	var updateHandleBox = function (event, ui) {
		$(this).data().slider.element.find(".ui-slider-handle").text(ui.value);
	};

	//hour slider
	$('#hour-slider').slider({
		range: 'min',
		min:0, 
		max:23,
		slide: updateHandleBox,
		value:0

	});

	//minute slider
	$('#minute-slider').slider({
		range: 'min',
		min:0,
		max:59,
		slide: updateHandleBox,
		vlaue:0
	});

	//second slider
	$('#second-slider').slider({
		range: 'min',
		min:0,
		max:59,
		slide: updateHandleBox,
		value:0
	});

	//Display the initial time value
	$('#time-display').html('00:00:00');

	//Update slider handle location and value
	function updateSliders(periods) {
		var hour = periods[4];
		var minute = periods[5];
		var second = periods[6];

		$('#hour-slider').slider('option', 'value', hour);
		$('#minute-slider').slider('option', 'value', minute);
		$('#second-slider').slider('option', 'value', second);

		$('#hour-slider').data().slider.element.find('.ui-slider-handle').text(hour);
		$('#minute-slider').data().slider.element.find('.ui-slider-handle').text(minute);
		$('#second-slider').data().slider.element.find('.ui-slider-handle').text(second);
	}

	//Function for staring a timer
	function startingTimer() {

		//Destroy a timer object that may be excuted previously
		$('#time-display').countdown('destroy');

		//Collect the slider values 
		var hour = '+' + $('#hour-slider').slider('value') + 'h ';
		var minute = '+' + $('#minute-slider').slider('value') + 'm ';
		var second = '+' + $('#second-slider').slider('value') + 's';

		//Combine time values for countdown.js
		var timeSet = hour + minute + second;

		//Run the countdown plug-in
		//It runs updateSliders function with every ticks
		//It runs noticeTimeUp function when the time is up
		$('#time-display').countdown({
			until: timeSet,
			layout: '{hnn}{sep}{mnn}{sep}{snn}',
			onTick: updateSliders, 
			onExpiry: noticeTimeUp
		});
	}

	//start timer
	$('#start-timer').click( function() {
		startingTimer();
	});

	//stop timer
	$('#stop-timer').click( function() {
		$('#time-display').countdown('pause');
	});

	//resume timer
	$('#resume-timer').click( function() {
		startingTimer();
	});

	//reset timer
	$('#reset-timer').click( function() {
		$('#time-display').countdown('destroy');
		$('#time-display').html('00:00:00');

		//Initialize the time set for countdown plug-in
		var periods = [0, 0, 0, 0, 0, 0, 0];

		updateSliders(periods);

	});
});